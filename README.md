

```bash
python3 -m venv docs_env
```


```bash
source docs_env/bin/activate
```

```bash
export PYTHONPATH=$PWD
uvicorn app.main:app --reload
```

```bash
alembic init alembic
```

```bash
alembic revision -m "inital migration" or alembic revision --autogenerate -m "foo"
```

```bash
 python3 backend_pre_start.py
 ```

 ```bash
 alembic upgrade head
```