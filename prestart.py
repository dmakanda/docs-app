import subprocess

import sys
import os

from alembic.config import Config
from alembic import command
from app.core.config import ROOT

alembic_cfg = Config(ROOT / "alembic.ini")

subprocess.run([sys.executable, os.path.join(ROOT, "app/backend_pre_start.py")])
command.upgrade(alembic_cfg, "head")
subprocess.run([sys.executable, os.path.join(ROOT, "app/initial_data.py")])