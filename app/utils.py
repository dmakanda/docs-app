import logging
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict, List, Optional, Union, Any

import emails
from emails.template import JinjaTemplate
from jose import jwt

from app.core.config import settings

def send_email(
    email_to: str,
    subject_template: str,
    html_template: str,
    environment: Dict[str, Any] = {}
) -> None:
    assert settings.EMAILS_ENABLED, 'no provider for sending emails'
    message = emails.Message(
        subject_template=JinjaTemplate(subject_template),
        html= JinjaTemplate(html_template),
        mail_from=(settings.EMAILS_FROM_NAME, settings.EMAILS_FROM_EMAIL),
    )
    smtp_options = {'host': settings.EMAILS_HOST, 'port': settings.EMAILS_PORT}
    if settings.EMAILS_TLS:
        smtp_options['tls'] = True
    if settings.EMAILS_USER:
        smtp_options['user'] = settings.EMAILS_USER
    if settings.EMAILS_PASSWORD:
        smtp_options['password'] = settings.EMAILS_PASSWORD
    response = message.send(
        to=email_to,
        render=environment,
        smtp=smtp_options)
    logging.info(f'send_email result {response}')

def send_reset_password_email(email_to: str, email: str, token: str) -> None:
    project_name = settings.PROJECT_NAME
    subject = f"{project_name} - Reset your password {email}"
    with open(Path(settings.EMAIL_TEMPLATES_DIR) / 'password_reset_email.html', 'r') as f:
        template_str = f.read()
    server_host = settings.SERVER_HOST
    link = f"{server_host}/reset-password?token={token}"
    send_email(
        email_to=email_to,
        subject_template=subject,
        html_template=template_str,
        environment={
            'project_name': project_name,
            'username': email,
            'email': email_to,
            'valid_hours': settings.EMAIL_RESET_TOKEN_EXPIRE_HOURS,
            'link': link
        }
    )

def send_new_account_email(email_to: str, username: str, password: str) -> None:
    project_name = settings.PROJECT_NAME
    subject = f"{project_name} - New account for user {username}"
    with open(Path(settings.EMAIL_TEMPLATES_DIR) / "new_account.html") as f:
        template_str = f.read()
    link = settings.SERVER_HOST
    send_email(
        email_to=email_to,
        subject_template=subject,
        html_template=template_str,
        environment={
            "project_name": settings.PROJECT_NAME,
            "username": username,
            "password": password,
            "email": email_to,
            "link": link,
        },
    )


def generate_password_reset_token(email: str) -> str:
    delta = timedelta(hours=settings.EMAIL_RESET_TOKEN_EXPIRE_HOURS)
    now = datetime.utcnow()
    expires = now + delta
    exp = expires.timestamp()
    encoded_jwt = jwt.encode(
        {"exp": exp, "nbf": now, "sub": email}, settings.SECRET_KEY, algorithm="HS256",
    )
    return encoded_jwt


def verify_password_reset_token(token: str) -> Optional[str]:
    try:
        decoded_token = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        return decoded_token["email"]
    except jwt.JWTError:
        return None

