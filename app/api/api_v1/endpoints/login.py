from datetime import timedelta
from typing import Any, Dict
from fastapi import APIRouter, Depends, HTTPException, status, Body
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from app.models import User
from app.crud import user as crud_user
from app.schema import Token, User, Msg
from app.api import deps
from app.core.config import settings
from app.core.security import create_access_token
from app.utils import (
    generate_password_reset_token,
    send_reset_password_email,
    verify_password_reset_token,
)

from app.core.security import get_password_hash

router = APIRouter()

@router.post("/login/access-token", response_model=Token)
def login_access_token(db: Session = Depends(deps.get_db), form_data: OAuth2PasswordRequestForm = Depends()):
    """
    OAuth2 compatible token login, get an access token for future requests
    """
    user = crud_user.authenticate(
        db,
        email=form_data.username,
        password=form_data.password,
    )
    if not user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Incorrect email or password",
        )
    if not crud_user.is_active(user):
        raise HTTPException(status_code=400, detail="Inactive user")
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    return {"access_token": create_access_token(
        user.id, expires_delta=access_token_expires
    ), "token_type": "bearer"}

@router.post("/login/test-token", response_model=User)
def test_token(current_user: User = Depends(deps.get_current_active_user)):
    return current_user

@router.post("/password-recovery/{email}", response_model=Msg)
def password_recovery(email: str, db: Session = Depends(deps.get_db)) -> Any:
    user = crud_user.get_by_email(db, email=email)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    token = generate_password_reset_token(email=email)
    send_reset_password_email(email_to=user.email, email=email, token=token)
    return {"message": f"Password recovery link sent to {email}"}


@router.post("/reset-password/", response_model=Msg)
def reset_password(
    token: str = Body(...),
    password: str = Body(...),
    db: Session = Depends(deps.get_db),
) -> Any:
    email = verify_password_reset_token(token)
    if not email:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid token")
    user = crud_user.get_by_email(db, email=email)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    elif not crud_user.is_active(user):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Inactive user")
    hashed_password = get_password_hash(password)
    user.hashed_password = hashed_password
    db.add(user)
    db.commit()
    return {"message": "password updated"}
