from sqlalchemy.orm import Session
import logging
from app.core.config import settings
from app.db import base #noqa: F401
from app import crud, models, schema

def init_db(db: Session) -> None:
    if settings.FIRST_SUPERUSER:
        user = crud.user.get_by_email(db, email=settings.FIRST_SUPERUSER)
        if not user:
            user_in = schema.UserCreate(
                email=settings.FIRST_SUPERUSER,
                password=settings.FIRST_SUPERUSER_PASSWORD,
                is_admin=True,
            )
            user = crud.user.create(db, obj_in=user_in) # noqa: F841
    else:
        logging.info("No superuser found, creating one")
   
