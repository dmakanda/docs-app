"""inital migration

Revision ID: 4610798464de
Revises: 
Create Date: 2023-05-28 01:52:53.528051

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4610798464de'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
