"""inital migration 2

Revision ID: cccb621c9c39
Revises: 4610798464de
Create Date: 2023-05-28 01:57:47.512050

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cccb621c9c39'
down_revision = '4610798464de'
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
